import _ from 'lodash';
import * as Handy from '@ohdoki/handy-sdk';

const CONNECTION_KEY = "YOUR_CONNECTION_KEY";

const handy = Handy.init({
    syncClientServerTime: false
});


handy.on('connect', () => {
    console.log('connected');
    goUpThenDown();
});


handy.connect(CONNECTION_KEY);


async function goUpThenDown() {
    console.log('Going up');
    await handy.hdsp(100, 0, "percent", "percent", true);

    setTimeout(async () => {
        console.log('Going down');
        await handy.hdsp(10, 400, "absolute", "absolute", true);
    }, 2000);

}


