const path = require("path");


module.exports = {
    mode: "development",
    entry: "./src/demo.ts",
    output: {
        filename: "main.js",
        path: path.resolve(__dirname, 'dist')
    }
}