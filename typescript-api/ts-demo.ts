import { HandyApi } from "./_HANDYAPI/HandyApi";
import { ErrorResponse } from "./_HANDYAPI/models/ErrorResponse";
import { HAMPStartResponse } from "./_HANDYAPI/models/HAMPStartResponse";
import { HAMPStopResponse } from "./_HANDYAPI/models/HAMPStopResponse";
import { Mode } from "./_HANDYAPI/models/Mode";
import { ModeUpdateResponse } from "./_HANDYAPI/models/ModeUpdateResponse";
import { StateResult } from "./_HANDYAPI/models/StateResult";

const CONNECTION_KEY = "YOUR_CONNECTION_KEY";

const handyApi = new HandyApi();

async function startAndStopHandy() {
    console.log('Setting mode to HAMP');
    const modeRes = await handyApi.base.setMode(CONNECTION_KEY, {
        mode: Mode.HAMP
    });
    console.log("modeRes:", modeRes);
    if ((modeRes as ErrorResponse).error) {
        throw new Error("Failed to set mode. Error: " + (modeRes as ErrorResponse).error);
    } else if ((modeRes as ModeUpdateResponse).result === ModeUpdateResponse.result.SUCCESS_NEW_MODE ||
        (modeRes as ModeUpdateResponse).result === ModeUpdateResponse.result.SUCCESS_SAME_MODE) {
        console.log('Mode set successfully');
    } else {
        throw new Error("Failed to set mode");
    }

    console.log('Starting HAMP');
    const startHampRes = await handyApi.hamp.start(CONNECTION_KEY);
    console.log("startHampRes:", startHampRes);

    if ((startHampRes as ErrorResponse).error) {
        throw new Error("Failed to start HAMP. Error: " + (startHampRes as ErrorResponse).error);
    } else if ((startHampRes as HAMPStartResponse).result === StateResult.SUCCESS_NEW_STATE || (startHampRes as HAMPStartResponse).result === StateResult.SUCCESS_SAME_STATE) {
        console.log('Hamp started');
    } else {
        throw new Error("Failed to start HAMP");
    }

    await sleep(5000);

    console.log('Stopping HAMP');
    const stopHampRes = await handyApi.hamp.hampStop(CONNECTION_KEY);
    console.log("stopHampRes:", stopHampRes);

    if ((stopHampRes as ErrorResponse).error) {
        throw new Error("Failed to stop HAMP. Error: " + (stopHampRes as ErrorResponse).error);
    } else if ((stopHampRes as HAMPStopResponse).result === StateResult.SUCCESS_NEW_STATE || (stopHampRes as HAMPStopResponse).result === StateResult.SUCCESS_SAME_STATE) {
        console.log('HAMP stopped');
    } else {
        throw new Error("Failed to stop HAMP");
    }

}



async function start() {
    try {
        await startAndStopHandy();
    } catch (err) { console.error(err) }
}
start();


async function sleep(duration: number) {
    return new Promise(resolve => setTimeout(resolve, duration));
}