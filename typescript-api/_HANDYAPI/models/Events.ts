/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum Events {
    CONNECTED = 'connected',
    DISCONNECTED = 'disconnected',
}
