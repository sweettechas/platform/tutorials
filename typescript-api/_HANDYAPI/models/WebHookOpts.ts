/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Events } from './Events';

export type WebHookOpts = {
    url?: string;
    events?: Array<Events>;
};
