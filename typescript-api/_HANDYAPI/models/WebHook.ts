/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { WebHookOpts } from './WebHookOpts';

export type WebHook = (WebHookOpts & {
id: string;
expires?: number;
});
