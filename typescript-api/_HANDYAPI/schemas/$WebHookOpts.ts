/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $WebHookOpts = {
    properties: {
        url: {
    type: 'string',
},
        events: {
    type: 'array',
    contains: {
        type: 'Events',
    },
},
    },
} as const;
