/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $WebHook = {
    type: 'all-of',
    contains: [{
    type: 'WebHookOpts',
}, {
    properties: {
        id: {
    type: 'string',
    isRequired: true,
},
        expires: {
    type: 'number',
},
    },
}],
} as const;
