//comment out the next line if you are in a browser
const fetch = require('node-fetch'); //This only works with node-fetch v2! npm install node-fetch@2

let TestimatedAverageOffset = 0;

async function getServerTime() {
    const response = await fetch('https://www.handyfeeling.com/api/handy/v2/servertime', {
        headers: {
            "accept": "application/json",
            "Content-Type": "application/json",
        },
    });
    const data = await response.json();//Example response { serverTime: 1668517713525 }
    return data.serverTime;
}

async function syncServerTime(syncTries) {
    let offsetAggregated = 0;
    for (let index = 0; index < syncTries; index++) {
        const Tstart = Date.now();
        const Tserver = await getServerTime();
        const Tend = Date.now();
        const Trtd = Tend - Tstart;
        const Toffset = (Tserver + (Trtd / 2)) - Tend;
        offsetAggregated += Toffset;
    }
    TestimatedAverageOffset = Math.round(offsetAggregated / syncTries);
    console.log('Server clock is synced');
    console.log("Estimated average offset:", TestimatedAverageOffset);

}

//Call this function any time you want 
function getEstimateServertime() {
    const TestimatedServertime = Date.now() + TestimatedAverageOffset;
    return TestimatedServertime;
}


syncServerTime(10); //Sync the clock (only needs to be done once) - Here with 10 requests
